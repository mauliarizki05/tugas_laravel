<?php       
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $object = new animal ("shaun");

    echo "Name  : $object->name <br>";
    echo "legs  : $object->legs <br>";
    echo "Cold blooded : $object->cold_blooded <br><br>";


    $object3 = new frog ("buduk");
    echo "Name : $object3->name <br>";
    echo "legs : $object3->legs <br>";
    echo "Cold blooded : $object3->cold_blooded <br>";
    echo "Jump : Hop Hop<br><br>";

    $object2 = new ape ("kera sakti");
    echo "Name : $object2->name <br> ";
    echo "legs : $object2->legs <br> ";
    echo "Cold blooded : $object2->cold_blooded<br>";
    echo "Yell : Auooo";

