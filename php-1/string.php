<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php
    //SOAL NO 1.   
    $string = "PHP is never old";
    $panjangkalimat = strlen($string);
    $jumlahkata = str_word_count($string);
    echo "kalimat : $string <br>";
    echo "panjang string : $panjangkalimat <br>";
    echo "jumlah kata : $jumlahkata <br>";

     //SOAL NO 2
       
        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        echo "Kata kedua: " . substr($string2, 2, 5);
        echo "<br> Kata Ketiga: ".substr ($string2, 7, 9). "<br>";

        echo "<h3> Soal No 3 </h3>";

        
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but sexy!";
        echo "String: \"$string3\" <br>";
        echo "String akan diganti: ".str_replace("sexy", "awesome", $string3);

    ?>
</body>
</html>