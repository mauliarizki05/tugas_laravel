@extends('adminlte.master')

@section('content')
<div class="ml-5 mt-5">
</div>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create New User</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/users" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">
        </div>
        <div class="form-group">
          <label for="body">Body</label>
          <input type="text" class="form-control" id="body" name="body" placeholder="Body">
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
  </div>
@endsection