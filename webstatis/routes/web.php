<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@kirim');


Route::get('/data-table', function(){
    return view('table.data.table');
});



//CRUD KATEGORI
ROUTE::get('/cast', 'CastController@index' );
ROUTE::get('/cast/create', 'CastController@create' );
ROUTE::post('/cast', 'CastController@store');
ROUTE::get('/cast/{cast_id}','CastController@show');
ROUTE::get('/cast/{cast_id}/edit', 'CastController@edit');
ROUTE::put('/cast/{cast_id}', 'CastController@update');
ROUTE::delete('/cast/{cast_id}', 'CastController@destroy');