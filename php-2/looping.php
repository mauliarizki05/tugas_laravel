<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
<!-- No.1 -->
    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        echo "<h4> LOOPING PERTAMA </h4>";
        for ($i= 2 ; $i <= 20;  $i+=2) {
            echo $i. " - I Love PHP <br>";
        }
        echo "<h4> LOOPING KEDUA </h4>";
        for ($j= 20; $j >= 1;  $j-= 2) {
            echo $j. " - I Love PHP <br>";
        }
// <No class="2"></No>

        echo "<h3>LOOPING 2</h3>";
        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        echo "<br>";
        foreach ($numbers as $num){
        $rest[]= $num *5;
            
        }
        print_r($rest);
    
// <No class="3"></No>
        echo "<h3> LOOPING 3 </h3>";
        $items= [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        

        foreach ($items as $key => $val){
            $items = array(
                'id' =>              $val[0],
                'name' =>            $val[1],
                'prince' =>          $val[2],
                'description' =>     $val[3],
                'source' =>          $val[4],
            );
            print_r($items);
            echo "<br>";
        }
// <No class="4"></No>
    echo "<h3> LOOPING 3 </h3>";
    $star=5;
    for($a=$star;$a>0;$a--){
    for($b=$star;$b>=$a;$b--){
        echo "*";
    }
    echo "<br>";
    }


        ?>


  
</body>
</html>